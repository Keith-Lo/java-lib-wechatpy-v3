package wechatv3_sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.xml.bind.DatatypeConverter;

import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Wechat Pay API V3 (Java)
 * 
 * Please notice that this is an non official API SDK
 * Official SDK only offered PHP Guzzle plugin (https://github.com/wechatpay-apiv3/wechatpay-guzzle-middleware)
 * 
 * For Version 3 official documentation please check out (https://pay.weixin.qq.com/wiki/doc/api/micropay.php?chapter=1_1) 
 * 
 * @author Keith Lo
 * @date 2019-12-23
 */
public class WechatPayV3 {
	
	public JSONObject params = new JSONObject();
		
	public WechatPayV3() throws RuntimeException {
		this.init();
	}
	
	public void init() throws RuntimeException {
		ReadProperties();
		InitAPIMap();
	}

	public JSONObject execute(int apiName) throws Exception {
		if( !API_MAP.containsKey(new Integer(apiName)) ) {
			throw new RuntimeException("API not found. Given code : "+apiName);
		}		
		
		this._addLog("Execute WechatPayV3 API : Begin");
		WechatPayAPI api = API_MAP.get(new Integer(apiName));
		String query = this._getUriQuery(api.query, api.properties);
		String nonce = this._generateNonce();
		long now = Long.valueOf(System.currentTimeMillis() / 1000);
		
//		nonce = "zkfhR72w1VkcEtsNRXgfd752QaM9jPG2";
//		now = 1577675306;
		String signature = this._sign(this._getSigningContent(api.method, query, nonce, now));
//		System.out.println(signature);
//		System.exit(0);
		String authorization = this._getAuthorizationToken(signature, nonce, now);
		
		JSONObject result = this._sendRequest(api.method, query, authorization);
		
		this._addLog("Execute WechatPayV3 API : End");		
		
		return result;
	}
	
	public JSONObject execute(int apiName, HashMap<String, String> params) throws Exception {
		this.setParams(params);
		
		return this.execute(apiName);
	}
	
	public JSONObject execute(int apiName, JSONObject params) throws Exception {
		this.setParams(params);
		
		return this.execute(apiName);
	}
	
	public void setParams(HashMap<String, String> params) {
		this.params = new JSONObject(params);
	}
	
	public void setParams(JSONObject json) {
		this.params = json;
	}
	
	public JSONObject getParams(){
		return this.params;
	}
	
	public String getProperty(String prop) {
		return this.getProperty(prop, null);
	}
	
	public String getProperty(String prop, String defaultValue) {
		if( WechatPayProperties.containsKey(prop) ) {
			return WechatPayProperties.get(prop);
		}
		
		return defaultValue;
	}
	
	protected JSONObject _sendRequest(String httpMethod, String query, String authorization) throws ClientProtocolException, IOException, JSONException {
		String endpoint = HTTP_METHOD + "://" + this.getProperty("host") + query;
		
		HttpRequestBase request = null;
		if( httpMethod == HTTP_GET ) {
			request = this._getHttpGet(endpoint, authorization);
		}else if( httpMethod == HTTP_POST ) {
			request = this._getHttpPost(endpoint, authorization);
		}else {
			throw new RuntimeException("WechatPayV3 does not support the http method : "+httpMethod);
		}
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
        this._addLog("----- Sending message to Wechat Pay V3 : BEGIN -----");
        this._addLog("REQ:");
        this._addLog(">>> " + request.getRequestLine().toString());
        this._addLog(">>> Headers:");
        for (Header header : request.getAllHeaders()) {
            this._addLog(">>>>> " + header.getName() + ": " + header.getValue());
        }
        if( httpMethod == HTTP_POST ) {
        	this._addLog(">>> Body:");
        	this._addLog(this.params.toString());
        }
        
		String response = httpClient.execute(request, new ResponseHandler<String>() {
            @Override
            public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                String message = entity != null ? EntityUtils.toString(entity) : null;
                
                if (status >= 200 && status < 300) {                   
                    return message;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status + ", message: " + message);
                }
            }
        });
		this._addLog("RES:");
		this._addLog("<<< " + response);	
		
		this._addLog("----- Sending message to Wechat Pay V3 : END -----");
		
		return new JSONObject(response);
	}
	
	protected HttpRequestBase _getHttpGet(String endpoint, String authorization) {
		HttpGet httpGet = new HttpGet(endpoint);
		
		httpGet.addHeader("Accept", HTTP_CONTENT_TYPE_JSON);
		httpGet.addHeader("Accept-Language", HTTP_ACCEPT_LANG);
		httpGet.addHeader("Authorization", authorization);
		httpGet.addHeader("Host", this.getProperty("host"));
		httpGet.addHeader("User-Agent", HTTP_USER_AGENT);
		
		return httpGet;
	}
	
	protected HttpRequestBase _getHttpPost(String endpoint, String authorization) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(endpoint);
		JSONObject json = this.params;
		
		httpPost.addHeader("Accept", HTTP_CONTENT_TYPE_JSON);
		httpPost.addHeader("Accept-Language", HTTP_ACCEPT_LANG);
		httpPost.addHeader("Authorization", authorization);
		httpPost.addHeader("Content-Type", HTTP_CONTENT_TYPE_JSON + ";charset=" + HTTP_ENCODING);
		httpPost.addHeader("Host", this.getProperty("host"));
		httpPost.addHeader("User-Agent", HTTP_USER_AGENT);
		
		httpPost.setEntity(new StringEntity(json.toString(), HTTP_ENCODING));
		
		return httpPost;
	}
	
	protected void _addLog(String message) {
		System.out.println(message);
	}
	
	protected String _getAuthorizationToken(String signature, String nonce, long now) throws UnsupportedEncodingException {
		String result = String.format("%s mchid=\"%s\",nonce_str=\"%s\",signature=\"%s\",timestamp=\"%s\",serial_no=\"%s\"", 
				this.getProperty("authorization"),
				this.getProperty("merchantId"),
				nonce,
				signature,
				now,
				this.getProperty("serialNumber")
				);

		return result;
	}
	
	protected String _getUriQuery(String query, HashMap<String, String> properties) throws RuntimeException, JSONException {
		ArrayList<String> keysForRemove = new ArrayList<String>();
		
		//Pushing default this.params value from properties file if exists
		for( Entry<String, String> entry : properties.entrySet() ) {
			String fromKey = entry.getKey();
			String toKey = entry.getValue();
			String value = null;
			
			if( this.getProperty(fromKey, null) != null ) {
				value = this.getProperty(fromKey);
				
				this.params.put(toKey, value);	//Push into this.params
			}
		}
		
		//Replace URI query string parameter to value.
		for( Entry<String, String> entry : properties.entrySet() ) {
			String fromKey = entry.getKey();
			String toKey = entry.getValue();
			String value = null;
			
			if( this.params.has(toKey) ) {
				value = this.params.getString(toKey);
				query = this.replaceQuery(query, fromKey, value, toKey);		
				keysForRemove.add(toKey);
			}		
		}
		
		if( !keysForRemove.isEmpty() ) {
			for( String removeKey : keysForRemove ) {
				if( this.params.has(removeKey) ) {
					this.params.remove(removeKey);
				}
			}
		}
		
		this._addLog("Query String : " + query);
		
		return query;
	}
	
	protected String replaceQuery(String query, String fromKey, String value, String toKey) {
		try {
			value = URLEncoder.encode(value, HTTP_ENCODING);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Unsupported encoding : " + HTTP_ENCODING);
		}
		String target = String.format("\\{%s\\}", toKey);
		
		return query.replaceAll(target, value);
	}
	
	protected String _generateNonce() {
		StringBuilder nonce = new StringBuilder();
		Random rand = new Random();
		for( int i =0; i < NONCE_LENGTH; i++ ) {
			int randomNum =rand.nextInt(NONCE_CHAR.length());
			nonce.append(NONCE_CHAR.charAt(randomNum));
		}
		
		return nonce.toString();
	}
	
	protected String _sign(String content) throws Exception {
		System.out.println("----");
		System.out.println(content);
		System.out.println("----");
		PrivateKey privateKey = this._readPrivateKey(this._readFile(this.getProperty("key")));
		 
		Signature signature = Signature.getInstance("SHA256withRSA");
		signature.initSign(privateKey);
		signature.update(content.getBytes());
		byte[] result = signature.sign();
		 
		return DatatypeConverter.printBase64Binary(result);
	}
	
	protected String _getSigningContent(String httpMethod, String query, String nonce, long now) {		 
		 JSONObject json = this.params;
		 String queryString = ( json != null && ( json.length() > 0 ) ) ? json.toString() + "\n" : "\n";
		 
		 String forSign = httpMethod + "\n"
				 + query + "\n"
				 + now + "\n"
				 + nonce + "\n"
				 + queryString;

		return forSign;
	}
	
	protected String _readFile(String pathname) throws FileNotFoundException {
	    File file = new File(pathname);
	    this._addLog(pathname);
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {
	            fileContents.append(scanner.nextLine() +lineSeparator);
	        }
	        return fileContents.toString();
	    }catch( Exception e) {
	    	e.printStackTrace();
	    } finally {
	        scanner.close();
	    }
		return null;
    }
	
	protected PrivateKey _readPrivateKey(String content) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String privKeyPEM = content.replace("-----BEGIN PRIVATE KEY-----", "");
        privKeyPEM = privKeyPEM.replace("-----END PRIVATE KEY-----", "");
//        System.out.println("Private key\n"+privKeyPEM);

        byte[] decoded = DatatypeConverter.parseBase64Binary(privKeyPEM);

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
        KeyFactory kf = KeyFactory.getInstance(KEY_ALGORITHM);
        return kf.generatePrivate(spec);
	}
	
	protected static void ReadProperties() throws RuntimeException {
		Properties properties = new Properties();
		
		try {
			properties.load(WechatPayV3.class.getClassLoader().getResourceAsStream("wechatpay.properties"));
		}catch( Exception e ) {
			throw new RuntimeException("Failed to load properties file. Please check configuration path. " + e.getMessage());
		}
		
		WechatPayProperties = new HashMap<String, String>();
		for( final String name: properties.stringPropertyNames() ) {
			WechatPayProperties.put(name, properties.getProperty(name));
		}
	}
	
	protected static void InitAPIMap() {
		API_MAP = new HashMap<Integer, WechatPayAPI>();	
		
		API_MAP.put(API_ORDER_ENQUIRY, _getOrderEnquiryAPI());
		API_MAP.put(API_QR_CODE_TRANSACTION, _getQrCodeTransactionAPI());
	}
	
	protected static WechatPayAPI _getOrderEnquiryAPI() {
		HashMap<String, String> propOrderEnquiry = new HashMap<String, String>();
		propOrderEnquiry.put("merchantId", "mchid");
		propOrderEnquiry.put("outTradeNo", "out_trade_no");
		WechatPayAPI api = new WechatPayAPI();
		api.put(HTTP_GET, "/hk/v3/transactions/out-trade-no/{out_trade_no}?mchid={mchid}", propOrderEnquiry);
		
		return api;
	}
	
	protected static WechatPayAPI _getQrCodeTransactionAPI() {
		HashMap<String, String> propOrderEnquiry = new HashMap<String, String>();
		propOrderEnquiry.put("merchantId", "mchid");
		propOrderEnquiry.put("appId", "appid");
		
		WechatPayAPI api = new WechatPayAPI();
		api.put(HTTP_POST, "/hk/v3/transactions/native", propOrderEnquiry);
		
		return api;
	}

	protected static HashMap<Integer, WechatPayAPI> API_MAP;
	protected static HashMap<String, String> WechatPayProperties;
	
//	HTTP Methods : Begin
	protected static final String HTTP_METHOD = "https";
	protected static final String HTTP_GET = "GET";
	protected static final String HTTP_POST = "POST";
	protected static final String HTTP_ENCODING = "UTF-8";
	protected static final String HTTP_CONTENT_TYPE_JSON = "application/json";
	protected static final String HTTP_ACCEPT_LANG = "en";
	protected static final String HTTP_USER_AGENT = "curl/7.35.0";
//	HTTP Methods : End
	
//	Keys & Encryption : Begin
	protected static final String NONCE_CHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	protected static final int NONCE_LENGTH = 32;
	protected static final String KEY_ALGORITHM = "RSA";
//	Keys & Encryption : End
	
//	API Map : Begin
	public static final int API_ORDER_ENQUIRY = 0;
	public static final int API_QR_CODE_TRANSACTION = 1;
//	API Map : End
}



class WechatPayAPI{
	public String method;
	public String query;
	public HashMap<String, String> properties;
	
	public void put(String method, String query, HashMap<String, String> properties) {
		this.method = method;
		this.query = query;
		this.properties = properties;				
	}
}