package wechatv3_sample;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONObject;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Start testing WechatPayV3");
		
		WechatPayV3 v3 = new WechatPayV3();
		try {		
			testOrderEnquiry(v3);
//			testQrCodeGenerate(v3);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Catched");
			e.printStackTrace();
			
		}
		
		System.out.println("End of the programme.");
	}
	
	protected static void testOrderEnquiry(WechatPayV3 v3) throws Exception {
		HashMap<String, String> prop = new HashMap<String, String>();
		prop.put("out_trade_no", "1234567890");
		
		JSONObject json = v3.execute(WechatPayV3.API_ORDER_ENQUIRY, prop);		
		System.out.println(json);
	}
	
	protected static void testQrCodeGenerate(WechatPayV3 v3) throws Exception {		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		final long ONE_MINUTE_IN_MILLIS = 60000;
		int minutes = 15;
		
		Date startDate = new Date();
		Date endDate = new Date(startDate.getTime() + ( minutes * ONE_MINUTE_IN_MILLIS )); 
		
		JSONObject prop = new JSONObject();
		prop.put("out_trade_no", "1234567890");
		prop.put("description", "ESHOP");
		prop.put("attach", "MY_CUSTOM_TAGS");
		prop.put("notify_url", "http://MY_NOTIFY_URL");
		prop.put("trade_type", "NATIVE");
		prop.put("time_start", formatter.format(startDate));
		prop.put("time_expire", formatter.format(endDate));
		prop.put("merchant_category_code", "4815");
		
		JSONObject amountProp = new JSONObject();
		amountProp.put("currency", "HKD");
		amountProp.put("total", 100);	//$1 HKD
		prop.put("amount", amountProp);
		
		
		
		System.out.println(prop);
		JSONObject response = v3.execute(WechatPayV3.API_QR_CODE_TRANSACTION, prop);		
		System.out.println(response);		
	}

}
